# -*- coding: utf-8 -*-
"""
Created on Tue Oct 28 00:13:28 2014

@author: Jose Capriles
"""

from Tkinter import Tk, Canvas, Frame, BOTH

from ball import Ball
import socket

#from player import Player

class PongGUI(Frame):
  
    #List to check all balls
    balls = []
    server = True

    msgscount = 1
    setup = False
    waiting = False
    buffsize = 1024

 
    def __init__(self, parent,screen=[600,400]):
        Frame.__init__(self, parent)   
        self.parent = parent     
        self.bg_status = 0
        self.create_ui()

        self.bind_all('<KeyPress-space>',self.start)
        
        self.bind_all('<Escape>',self.end)

        
    def end(self, event):
        self.master.destroy()
    
    def start(self, event):
        print "Starting"
        if self.server:
            self.create_server()
        else:
            self.connect_server()
            
        self.after(200, self.update)

    def connect_server(self):

        self.setup = True
        host = 'localhost'
        port = 9000
        addr = (host, port)
    
        self.tcpclisock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcpclisock.connect(addr)
        print 'Connected to: ', addr
        self.tcpclisock.setblocking(0)
        self.parent.title("Pong Client "+'Connected to: '+ str(addr))

        
    def create_server(self):
        
        host = ''
        port = 9000
        addr = (host, port)
        self.setup = True
        
        print "makeServer():"
        
        self.tcpsersock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcpsersock.bind(addr)
        self.tcpsersock.listen(5)
        self.tcpclisock, addr = self.tcpsersock.accept()
        self.tcpclisock.setblocking(0)
        print 'Connected from: ', addr
        self.parent.title("Pong Server: " + 'Connected from: ' + str(addr))        

    def send(self):
        print 'sending status..'
        try:
            for i in self.balls:            
                centers = i.get_center()
                vel = i.get_velocity()
                data = str(centers[0]) + " " +  str(centers[1]) + " " + str(vel[0]) + " " +  str(vel[1])
                print data
                self.tcpclisock.send(data)
                self.waiting = True
                
        except:
            print 'You need to connect to a server.'

    def create_ui(self):
        self.parent.title("Pong Server")        
        self.pack(fill=BOTH, expand=1)
        
        self.canvas = Canvas(self, bg="#"+str(self.bg_status)+str(self.bg_status)+str(self.bg_status))
        self.balls.append(Ball(self.canvas,1,[70,70],10,[2,-2]))
        self.canvas.pack(fill=BOTH, expand=1)


    def update(self):
        
        if self.waiting:
            self.balls = []

        if self.setup and not self.balls:
            try:
                received = self.tcpclisock.recv(self.buffsize)
                rec_splits = str(received).split() 
                print rec_splits 
                self.balls.append(Ball(self.canvas,1,[580,int(rec_splits[1])],10,[int(rec_splits[2]),int(rec_splits[3])]))
            except:
                pass
        
        if self.balls:
            self.waiting = False
        
        for i in self.balls:
            i.update()
            
            if i.get_position()[1] <= 0 or i.get_position()[1] >= 380:
                i.set_velocity([i.vel[0],-i.vel[1]])
       
            if i.get_position()[2] >= 600:
                if self.server and not self.waiting:
                    #Send to other 
                    print "===============Send============"
                    self.send()
                    i.set_position([650,0]) 
                    i.set_velocity([0,0])
                    self.waiting = True
            
            elif i.get_position()[0] <= 0:
                if self.server:
                    i.set_velocity([-i.vel[0],i.vel[1]])
                    
                    
        self.after(10, self.update)
 


def main():

    screen=[600,400]  
    root = Tk()
    
    pong = PongGUI(root,screen)
    
    root.geometry(str(screen[0])+"x"+str(screen[1])+"+500+300")
    root.mainloop()  


if __name__ == '__main__':
    main() 
