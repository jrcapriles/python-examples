A bouncing ball in a Canvas and jumps through the network to a different screen (left or right depending on server-client).

To run this example:
1- In a terminal start the server program
2- In a different terminal start the client program

At this point you should have both programs connected together through a TCP/IP socket. The header of the screen should change to "Connected to x.x.x.x"

3- Click the server's screen and press the "Space" key from your keyboard
4- Click the client's screen and press the "Space" key from your keyboard

Now the ball should start moving. Once the ball reaches the right side of the server screen or the left side of the client screen, a message will be sent between the applications and you should be able to see the ball in the other screen.

