#!/usr/bin/env python
#    
#                         License Agreement
#               For Open Source Computer Vision Library
#                       (3-clause BSD License)
#
#           Modifications (Corner detections) made by Jose Capriles 

import numpy as np
import cv2
import video

help_message = '''
USAGE: opt_flow.py [<video_source>]

Keys:
 1 - toggle HSV flow visualization
 2 - toggle glitch
 3 - corner detection (Harris)
 4 - corner detection (Morphological)

'''

def harris_corner_detection(img):
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        gray = np.float32(gray)
        dst = cv2.cornerHarris(gray,2,3,0.04)
        dst = cv2.dilate(dst,None)
        img[dst>0.01*dst.max()]=[0,0,255]
        return img

def binary_vertex_detector(img):
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)    
        kernel = np.ones((5,5), np.uint8)
        closing = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel)
        opening = cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernel)
        vertex = closing - opening
        #erode = cv2.erode(vertex, kernel)
        dilate = cv2.dilate(vertex, kernel)
        img[dilate<0.25]=[0,0,255]
        return img
    
def draw_corner(img):
    corner = harris_corner_detection(img)
#    vis = cv2.cvtColor(corner, cv2.COLOR_GRAY2BGR)
    return corner #vis

def draw_corner_morph(img):
    vis = binary_vertex_detector(img)
    #vis = cv2.cvtColor(vertex, cv2.COLOR_GRAY2BGR)
    return vis

def draw_flow(img, flow, step=16):
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1)
    fx, fy = flow[y,x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(vis, lines, 0, (0, 255, 0))
    for (x1, y1), (x2, y2) in lines:
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    return vis

def draw_hsv(flow):
    h, w = flow.shape[:2]
    fx, fy = flow[:,:,0], flow[:,:,1]
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx*fx+fy*fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[...,0] = ang*(180/np.pi/2)
    hsv[...,1] = 255
    hsv[...,2] = np.minimum(v*4, 255)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return bgr

def warp_flow(img, flow):
    h, w = flow.shape[:2]
    flow = -flow
    flow[:,:,0] += np.arange(w)
    flow[:,:,1] += np.arange(h)[:,np.newaxis]
    res = cv2.remap(img, flow, None, cv2.INTER_LINEAR)
    return res

if __name__ == '__main__':
    import sys
    print help_message
    try: fn = sys.argv[1]
    except: fn = 0

    cam = video.create_capture(fn)
    ret, prev = cam.read()
    prevgray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)
    show_hsv = False
    show_glitch = False
    show_corner = False
    show_corner_morph = False
    cur_glitch = prev.copy()

    while True:
        ret, img = cam.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(prevgray, gray, 0.5, 3, 15, 3, 5, 1.2, 0)
        prevgray = gray

        cv2.imshow('flow', draw_flow(gray, flow))
        if show_hsv:
            cv2.imshow('flow HSV', draw_hsv(flow))
        if show_glitch:
            cur_glitch = warp_flow(cur_glitch, flow)
            cv2.imshow('glitch', cur_glitch)
        if show_corner:
            cv2.imshow('corner', draw_corner(img))
        if show_corner_morph:
   	    cv2.imshow('corner morph', draw_corner_morph(img))

        ch = 0xFF & cv2.waitKey(5)
        if ch == 27:
            break
        if ch == ord('1'):
            show_hsv = not show_hsv
            print 'HSV flow visualization is', ['off', 'on'][show_hsv]
        if ch == ord('2'):
            show_glitch = not show_glitch
            if show_glitch:
                cur_glitch = img.copy()
            print 'glitch is', ['off', 'on'][show_glitch]
        if ch == ord('3'):
            show_corner = not show_corner
            print 'Corner detection is',['off', 'on'][show_corner]
        if ch == ord('4'):
            show_corner_morph = not show_corner_morph
            print 'Morphological corner detection is',['off','on'][show_corner_morph]

    cv2.destroyAllWindows()

