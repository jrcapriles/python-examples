# -*- coding: utf-8 -*-
"""
Created on Fri Sep 12 21:49:54 2014

@author: Jose Capriles
"""

from scipy import misc
from scipy import ndimage
import matplotlib.pyplot as plt
import numpy as np


image = misc.imread('texas.png', True)
lx, ly = image.shape

#Geometrical transformations:
# Cropping
crop_image = image[lx / 4: - lx / 4, ly / 4: - ly / 4]

# Upside down flip
flip_ud_image = np.flipud(image)

# Rotation
rotate_image = ndimage.rotate(image, 45)

# Rotation no reshape
rotate_image_noreshape = ndimage.rotate(image, 45, reshape=False)

print "figure 1: Basic Geometric trasnformation"

img = plt.figure()#figsize=(12.5, 2.5))
sub1 = plt.subplot(151)
sub1.imshow(image, cmap=plt.cm.gray)
sub1.axis('off')
sub1.set_title('Original')
sub2 = plt.subplot(152)
sub2.imshow(crop_image, cmap=plt.cm.gray)
sub2.axis('off')
sub2.set_title('Cropping')
sub3 = plt.subplot(153)
sub3.imshow(flip_ud_image, cmap=plt.cm.gray)
sub3.axis('off')
sub3.set_title('Upside Down')
sub4 = plt.subplot(154)
sub4.imshow(rotate_image, cmap=plt.cm.gray)
sub4.axis('off')
sub4.set_title('Rot')
sub5 = plt.subplot(155)
sub5.imshow(rotate_image_noreshape, cmap=plt.cm.gray)
sub5.axis('off')
sub5.set_title('Rot w/reshape')

plt.subplots_adjust(wspace=0.02, hspace=0.3, top=1, bottom=0.1, left=0, right=1)
plt.show()


# Edge detection
print "figure 2: Pattern matching"
fig = plt.figure()

im = np.zeros((256, 256))
im[64:-64, 64] = 1
im[64:-64, -64] = 1
im[120, 30:-30] = 1
im[30, 30:-30] = 1
im[20:-100, -104] = 1

im_match = 0.075*im
#im = ndimage.rotate(im, 15, mode='constant')
#im = ndimage.gaussian_filter(im, 8)

sub1 = plt.subplot(1,3,1)
sub1.imshow(im, cmap=plt.cm.gray)
sub1.axis('off')
sub1.set_title('Original Image')

# Image size
x, y = im.shape

pattern = np.zeros((10,10))
pattern[0:10, 5] = 1
pattern[5, 0:10] = 1
px, py = pattern.shape

sub2 = plt.subplot(1,3,2)
sub2.imshow(pattern, cmap=plt.cm.gray)
sub2.axis('off')
sub2.set_title('Pattern')

num_matches = 0
for i in range(0,(y-py)):
    for j in range(0,(x-px)):
        im_test = im[i:(i+px), j:(j+py)]
        if(np.array_equal(pattern, im_test)):
            print "There's a match at", i,j
            #im_match[i:(i+50),j:(j+50)]=pattern
            im_match[i:(i+px), j:(j+py)] = pattern 
            num_matches += 1

if(num_matches==0):
    print "There's no matches"
else:
    print num_matches, "match(es)"
        
sub3 = plt.subplot(1,3,3)
sub3.imshow(im_match, cmap=plt.cm.gray)
sub3.axis('off')
sub3.set_title('Result')

plt.show()
