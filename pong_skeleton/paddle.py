# -*- coding: utf-8 -*-


import Tkinter as tk

'''
Class Paddle
'''
class Paddle( object ):
    
    def __init__(self, canvas, size=1, pos= [10, 10], width = 10, length = 30, color="#fb0"):
        self.size = size
        self.pos = pos
        self.width = width
        self.length = length
        self.color = color
        self.canvas = canvas
        self.paddle = self.canvas.create_rectangle(pos[0], pos[1], pos[0]+width,pos[1]+length , outline=color, fill=color)
        
    def update_position(self, delta_pos):
        """
        Update current paddle position
        """
        
    def check_collision(self, ball):
        """
        Check collision between external object(ball) and paddle
        Expected return True/False
        """
        pass
        
    def get_border(self):
        """
        Return paddle's borders
        """
        pass

    def set_position(self,pos):
        """
        Set paddle's position
        """
        pass
        
    def get_position(self):
        """
        Return paddle's position
        """
        pass
        
    def set_width(self,width):
        """
        Set paddle's width
        """
        pass
        
    def get_width(self):
        """
        Return paddle's width
        """
        pass
    
    def set_length(self,length):
        """
        Set paddle's length/height
        """
        pass
        
    def get_length(self):
        """
        Return paddle's width
        """
        pass
    
    def set_color(self, color):
        """
        Set paddle's color
        """
        pass
    
    def get_paddle(self):
        """
        Return paddle 
        """
        