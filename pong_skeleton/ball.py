# -*- coding: utf-8 -*-
"""
Created on Mon Oct 27 23:48:51 2014

@author: Jose Capriles
"""

class Ball(object):
    
    def __init__(self, canvas, size=1, pos=[70,70], radious = 10, vel=[0,0], outline="black", fill="white"):
        
        self.size = size
        self.pos = pos
        self.radious = radious
        self.vel = vel
        self.canvas = canvas
        self.ball = self.canvas.create_oval(pos[0]-radious, pos[1]-radious, pos[0]+radious,pos[1]+radious, outline=outline, fill=fill, width=2)
        self.last_player = 'None'
        
    def set_position(self,pos):
        """
        Set ball's position
        """
        pass
        
    def get_center(self):
        """
        Returns ball center
        """
        pass
        
    def get_position(self):
        """
        Returns ball's position
        """
        pass
        
    def set_velocity(self,vel):
        """
        Set ball's velocity
        """
        pass

        
    def get_velocity(self):
        """
        Returns ball's velocity
        """
        pass
        
    def update(self, delta, last_player):
        """
        Updates ball
        """
        pass
