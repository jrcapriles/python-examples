# -*- coding: utf-8 -*-
"""
Created on Tue Oct 28 00:13:28 2014

@author: joser
"""

from Tkinter import Tk, Canvas, Frame, Label, BOTH

from paddle import Paddle
from ball import Ball

class PongGUI(Frame):
  
    def __init__(self, parent,screen=[600,400], r=20):
        Frame.__init__(self, parent)   
        self.parent = parent     
        self.initUI()
        self.bind_all('<Key>', self.key)
        
    def initUI(self):
      
        self.parent.title("Pong")        
        self.pack(fill=BOTH, expand=1)
        
        self.canvas = Canvas(self, bg="#000")
        
        
        """
        Create paddels
        """
        self.left = Paddle(self.canvas, 1, [30, 10], 10, 100,"#fb0")
        self.right = Paddle(self.canvas, 1, [570, 40], 10, 100,"#05f")        
        
        #self.up = Paddle(self.canvas, 1, [200, 40], 100, 10,"#f5f")        
        
        """
        Create Ball
        """
        self.ball = Ball(self.canvas,1,[70,70],10)        
        #self.ball = Ball(self.canvas,1,[200,200],20)        
        
        
        self.canvas.pack(fill=BOTH, expand=1)
        
        """
        Create Score
        """
        self.score_msg = '  Home  0 - 0  Visitor  '
        self.label_score = Label(self, text=self.score_msg, width=len(self.score_msg), bg='yellow')
        self.label_score.pack()
        
        """
        Start main loop
        """
        self.after(200, self.update)

    
    def key(self, event):
        """
        Check for key pressed and react accordingly:
            1 Player: 'Up' and 'Down' Keys
            2 Players: 'Up','Down' Keys for player 1 and 'w','s' Keys for player 2
            Update position paddles
        """
	print event.keysym
        pass
    
        
    def reset(self):
        """
        Resets the game
        """        
        pass
    
    def update(self):
        """
        This is the main loop:
            Update ball's position
            Increase speed when needed
            Update paddle's position
            Compute bounces
            Check collision with paddle
            Keep track goals
        """
        self.after(10, self.update)

 
    def update_score(self):
        """
        Update current score
        """
        pass
        


def main():

    screen=[600,400]  
    root = Tk()
    
    pong = PongGUI(root,screen)
    
    root.geometry(str(screen[0])+"x"+str(screen[1])+"+500+300")
    root.mainloop()  


if __name__ == '__main__':
    main() 
