Simple chat example using a Tkinter screen.

To run this example:
1- In a terminal start a GUI_chat program
2- Click the upper left corner "Connect" and select "Create server"
3- In a different terminal start another GUI_chat program
4- Click the upper left corner "Connect" and select "Join server"

At this point you should have both programs connected together through a TCP/IP socket. The header of the screen should change to "Connected to x.x.x.x"

5- Write the messages in the entry box and click "Send" 
