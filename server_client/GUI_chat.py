import Tkinter as tk
import socket


class Chat(tk.Tk):

    #Network variables
    msgscount = 1
    buffsize = 1024
    
    host = ''
    port = 9000
    buffsize = 1024
    addr = (host, port)
    setup = False

    def __init__(self):
        tk.Tk.__init__(self)
        self.geometry('400x270')
        self.create_ui()

    def create_ui(self):
         
         self.historial = tk.Listbox(self,width=49, height=13)
         self.historial.pack()
         
         self.msg = tk.Entry(self, bd=2, width=60, font='Arial 12 bold')
         self.msg.pack()
         
         self.send_btn = tk.Button(self, text='Send', font='Arial 15 bold', width=15, padx=5, pady=5, command=self.send)
         self.send_btn.pack()
         
         self.menubar = tk.Menu(self)
         self.connection_menu = tk.Menu(self.menubar, tearoff=0)
         self.connection_menu.add_command(label="Create Server", command=self.create_server)
         self.connection_menu.add_command(label="Join Server", command=self.connect_server)
         self.menubar.add_cascade(label="Connect", menu=self.connection_menu)
         self.config(menu=self.menubar)

         
    def get_address(self):
        return (self.host, self.port)
        
    def create_server(self):
        self.host = ''
        self.port = 9000
        print "Creating server.."
        self.addr = self.get_address()
        self.setup = True
        
        self.tcpsersock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcpsersock.bind(self.addr)
        self.tcpsersock.listen(5)
        self.tcpclisock, self.addr = self.tcpsersock.accept()
        print 'Connected from: ', self.addr        
        self.tcpclisock.setblocking(0)
        self.after(100, self.receive)

    
    def connect_server(self):
        print "Connecting to server.."
        self.host = 'localhost'
        self.port = 9000
        self.addr = self.get_address()
        self.setup = True

        self.tcpclisock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcpclisock.connect(self.addr)
        print 'Connected to: ', self.addr
        self.tcpclisock.setblocking(0)
        self.after(1000, self.receive)

    def send(self):
        print "Sending message"
        try:
            data = self.msg.get()
            self.historial.insert(self.msgscount, "-->" + data)
            self.msg.delete(0,tk.END)
            self.msgscount += 1
            self.tcpclisock.send(data)
        except:
            print 'No server connection found. Please create and join to a server.'


    def receive(self):
        if self.setup == True:
            try:
                received = self.tcpclisock.recv(self.buffsize)
                self.historial.insert(self.msgscount, received)
                self.msgscount += 1
            except:
                pass
            self.after(10, self.receive)
                
    
def main():
   
    app = Chat()
    app.mainloop()  
 
 
if __name__ == '__main__':
    main()  

