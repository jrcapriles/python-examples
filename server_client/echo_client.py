#!/usr/bin/env python 

""" 
A simple echo client that handles some exceptions 
""" 

import socket 
import sys 

#Communication parameters
host = 'localhost' 
port = 50000 
size = 1024 
s = None


while True: 
    #Try to connect with the server
    try: 
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        s.connect((host,port)) 
    except socket.error, (value,message): 
        if s: 
            s.close() 
        print "Could not open socket: " + message 
        sys.exit(1) 

    # Take the input from the user
    msg = raw_input("$: ")
    s.send(msg) 
    
    #Wait until message is received back
    data = s.recv(size) 
    
    print "Received: ", data

    s.close() 
