#!/usr/bin/env python 

""" 
A simple echo server that handles exceptions 
""" 

import socket
import sys 

#Communication parameters
host = '' 
port = 50000 
backlog = 5 
size = 1024 
s = None 

try: 
    #Try to create the server
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    s.bind((host,port)) 
    s.listen(backlog) 
except socket.error, (value,message): 
    if s: 
        s.close() 
    print "Could not open socket: " + message 
    sys.exit(1) 
    
while True:
 
    #Accept a connection from clients
    client, address = s.accept() 
    #Receive the data 
    data = client.recv(size)

    #Send data back to client     
    if data: 
        client.send(data) 
        
    client.close()
