# -*- coding: utf-8 -*-
"""
Created on Thu Oct 16 16:21:57 2014

@author: jrcapriles
"""

import Tkinter as tk
from BankAccount import *

TITLE_FONT = ("Helvetica", 10)

class Application(tk.Frame):
    
    bk1 = BankAccount(20)
    bk2 = BankAccount(20)
    bk3 = BankAccount(20)
    bk4 = BankAccount(20)
    bk5 = BankAccount(20)
    
    accounts = [bk1, bk2, bk3, bk4, bk5]    

    def check_input(self):    
    #Validate the inputs from the GUI        
        try:
            if isinstance( self.account.get() , (int,long) ):
                if 0 <= self.account.get() <= 4:
                    print "account id OK"
                else:
                    self.write(message="The account id must be an integer value between 0 and 4")
                    return False
        except ValueError:
            self.write(message="The account id must be an integer value between 0 and 4")
            return False
        
        try:
            if isinstance( float(self.amount.get()) , (int,long,float) ):
                print "amount OK"
        except ValueError:
            self.write(message="The amount must be an numeric value")
            return False
        
        return True

    
    def withdraw(self):
        if self.check_input():
            self.accounts[int(self.account.get())].withdraw(float(self.amount.get()))
            self.write()

    def deposit(self):
        if self.check_input():
            self.accounts[int(self.account.get())].deposit(float(self.amount.get()))
            self.write()
    
    def write(self, acc_id = None, message = None):
        if message is not None:
            self.text.insert(tk.END, str(message) + "\n")
        elif acc_id is None:
            self.text.insert(tk.END, self.accounts[int(self.account.get())].report(int(self.account.get())) +"\n")
        else:
            self.text.insert(tk.END, self.accounts[acc_id].report(acc_id) +"\n")
     
        self.text.see(tk.END)        
        self.text.update()
        
    def report(self):
        self.write(message="\n\n======== Bank accounts report =========")
        for i in range(0,5):
            self.write(acc_id=i)
        self.write(message="=======================================\n\n")
        
    def createWidgets(self):
        
        #Create sliders       
        self.label_account = tk.Label(self,text="Account # (0,1,2,3 or 4):", font=TITLE_FONT).grid(column=1,row=1)
        self.label_amount = tk.Label(self,text="Amount $", font=TITLE_FONT).grid(column=1,row=2)

        self.account = tk.IntVar()
        entry = tk.Entry(self,textvariable=self.account).grid(column=2,row=1)
        self.account.set("0")
        
        self.amount = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.amount).grid(column=2,row=2)
        self.amount.set("10.0")

        self.QUIT = tk.Button(self)
        self.QUIT["text"] = "QUIT"
        self.QUIT["fg"]   = "red"
        self.QUIT["command"] =  self.quit
        self.QUIT.grid(column = 4, row=0)

        self.withdraw_button = tk.Button(self)
        self.withdraw_button["text"] = "Withdraw",
        self.withdraw_button["command"] = self.withdraw
        self.withdraw_button.grid(column=4,row=2)

        self.deposit_button = tk.Button(self)
        self.deposit_button["text"] = "Deposit",
        self.deposit_button["command"] = self.deposit
        self.deposit_button.grid(column=3,row=2)
        
        self.report_button = tk.Button(self)
        self.report_button["text"] = "Report",
        self.report_button["command"] = self.report
        self.report_button.grid(column=5,row=2)
             
        self.text = tk.Text(self, width = 80, height = 10)
        self.text.grid(column=0, row=3, columnspan=6)
        
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        self.createWidgets()
        self.pack()

root = tk.Tk()

app = Application(master=root)
app.master.title("Bank account example")
app.mainloop()
root.destroy()
