# -*- coding: utf-8 -*-
"""
Created on Thu Oct 16 16:21:57 2014

@author: jrcapriles
"""


class BankAccount(object):
    def __init__(self, initial_balance=0):
        self.balance = initial_balance
    def deposit(self, amount):
        self.balance += amount
    def withdraw(self, amount):
        self.balance -= amount
    def overdrawn(self):
        return self.balance < 0
    def report(self, acc_id):
        return 'Account id ' + str(acc_id) + ' balance: ' + str(self.balance)
